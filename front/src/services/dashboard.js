/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {baseURL} from "./authentication"

/**
 * Makes a call to the API to get scheduled visits during a specific day.
 * @param API
 * @param token JWT
 * @param date Date during which we want to get scheduled visits
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function getScheduledVisits(API, token, date) {
	return await API(baseURL, "/scheduled_visits", {
		method: "get",
		query: {
			date: date
		},
	}, token)
}

/**
 * Makes a call to the API to search for worksites which satisfy the query
 * @param API
 * @param token JWT
 * @param query The query
 * @param signal {AbortSignal} AbortSignal used to abort a fetch request
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function searchWorksites(API, token, query, signal) {
	return await API(baseURL, "/worksites", {
		method: "get",
		query: {
			q: query
		},
		signal: signal
	}, token)
}
