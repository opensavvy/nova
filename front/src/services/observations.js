/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {baseURL} from "./authentication"

/**
 * Gets predefined observations from the API (of a specific type)
 * @param API
 * @param token JWT token
 * @param type Type of observation
 * @returns {Promise<*>}
 */
export async function getPredefinedObservations(API, token, type) {
	const response = await API(baseURL, "/predefined_observations", {
		method: "get",

	}, token)
	return response.filter(v => v.type.type === type).map(v => ({
		id: v.number,
		value: v.text
	}))
}

/**
 * Adds the specified text to observations and automatically inserts a line break if necessary
 * @param localObservations Previous observations
 * @param toAdd Text to add
 * @returns {*}
 */
export function addToObservations(localObservations, toAdd) {
	const separator = localObservations.length === 0 || localObservations.charAt(localObservations.length - 1) === '\n' ? '' : '\n'
	return localObservations + separator + toAdd
}
