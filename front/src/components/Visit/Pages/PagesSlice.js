/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {createSlice} from "@reduxjs/toolkit"

export const pagesSlice = createSlice({
	name: "progressBar",
	initialState: {},
	reducers: {
		initializePages: (state, action) => {
			if (!state[action.payload.event_id]?.steps) {
				return {
					...state,
					[action.payload.event_id]: {
						...state[action.payload.event_id],
						steps: action.payload.value
					}
				}
			}
		},
		addStep: (state, action) => {
			return {
				...state,
				[action.payload.event_id]: {
					...state[action.payload.event_id],
					steps: [
						...state[action.payload.event_id].steps,
						action.payload.value
					]
				}
			}
		},
		modifyStep: (state, action) => {
			state[action.payload.event_id].steps[action.payload.index] = action.payload.value
		},
		deleteStep: (state, action) => {
			state[action.payload.event_id].steps = state[action.payload.event_id].steps.filter((s, index) => index !== action.payload.index)
		},
		setInProgressStep: (state, action) => {
			state[action.payload.event_id].steps = state[action.payload.event_id].steps.map((s, index) => {
				if (s.status === "progress") {
					s.status = "next"
				}
				if (action.payload.index === index) {
					s.status = "progress"
				}
				return s
			})
		},
		removePagesData: (state, action) => {
			delete state[action.payload.event_id]
		}
	}
})

export const {initializePages, addStep, modifyStep, deleteStep, setInProgressStep, removePagesData} = pagesSlice.actions

export default pagesSlice.reducer
