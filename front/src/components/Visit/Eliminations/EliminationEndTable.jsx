/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import Td from "../../Table/Td"
import Table from "../../Table/Table"

export default function EliminationEndTable({initialSSOL, computedSSOL, initialSB, computedSB, onChange}) {
	return (
		<Table className={"text-center text-gray-700"} trs={[<tr key={"ext"}>
			<Td label={"Emplacement"}>
				<div className={"text-base"}>
					Extérieur
				</div>
			</Td>
			<Td label={"Lors de la visite"}>
				{initialSSOL}
			</Td>
			<Td label={"Après le constat"} className={"flex justify-center"}>
				<input
					type="text"
					className="focus:ring-indigo-500 focus:border-indigo-500 block sm:text-sm border-gray-300 rounded-md w-14 text-center"
					value={computedSSOL}
					onChange={v => onChange("computedSSOL", v.target.value)}
				/>
			</Td>
		</tr>, <tr key={"int"}>
			<Td label={"Emplacement"}>
				<div className={"text-base"}>
					Intérieur
				</div>
			</Td>
			<Td label={"Lors de la visite"}>{initialSB}</Td>
			<Td label={"Après le constat"} className={"flex justify-center"}>
				<input
					type="text"
					className="focus:ring-indigo-500 focus:border-indigo-500 block sm:text-sm border-gray-300 rounded-md w-14 text-center"
					value={computedSB}
					onChange={v => onChange("computedSB", v.target.value)}
				/>
			</Td>
		</tr>]} ths={[" ", "Lors de la visite", "Après le constat"]}/>
	)
}
