/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import ArrayTextBadge from "../../../ArrayTextBadge"
import {getFormattedDate} from "../../../../utils/functions"

export default function ConsumptionHistoryLabelItem({visit}) {
	return (
		<div className={"normal-case flex flex-col"}>
			{visit.startingEliminationPeriod &&
				<div className={"mb-2"}>
					<ArrayTextBadge color={"indigo"}>Début d'élimination</ArrayTextBadge>
				</div>}
			{visit.endingEliminationPeriod &&
				<div className={"mb-2"}>
					<ArrayTextBadge color={"indigo"}>Fin d'élimination</ArrayTextBadge>
				</div>}
			<div>
				{getFormattedDate(visit.scheduledAt)}
			</div>
		</div>
	)
}
