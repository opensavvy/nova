/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback} from 'react'
import Bin from "../../../../../Icons/Bin"
import StationLineContextMenuItem from "../StationLineContextMenuItem"
import {getRestrictedAttributes, getStationValue} from "../../../../../services/stations"

export default function RemoveStationContextMenuItem({
	                                                     station,
	                                                     index,
	                                                     type,
	                                                     event_id,
	                                                     allAttributes,
	                                                     excludedAttributes,
	                                                     updateStationLine,
	                                                     setIsPopupVisible,
	                                                     setPopupsOnClick
                                                     }) {

	/**
	 * Callback used when a click is made on temporary station removal
	 * @type {Function}
	 */
	const onClickTemporary = useCallback(() => {
		updateStationLine({
			event_id: event_id,
			type: type,
			index: index,
			value: getStationValue(getRestrictedAttributes(allAttributes, "isSuspended", excludedAttributes), station, ["isSuspended", "isRemoved"])
		})
	}, [updateStationLine, event_id, type, index, allAttributes, excludedAttributes, station])

	/**
	 * Callback used when a click is made on definitive station removal
	 * @type {Function}
	 */
	const onClickDefinitive = useCallback(() => {
		updateStationLine({
			event_id: event_id,
			type: type,
			index: index,
			value: getStationValue(getRestrictedAttributes(allAttributes, "isSuspended", excludedAttributes), station, ["isRemoved"])
		})
	}, [updateStationLine, event_id, type, index, allAttributes, excludedAttributes, station])


	return (
		<StationLineContextMenuItem station={station} index={index} className={"text-red-600"} type={type}
		                            event_id={event_id} icon={<Bin width={20}/>} attribute={"isRemoved"}
		                            label={"Démonter"} cancelLabel={'Annuler le démontage'}
		                            attributes={allAttributes}
		                            updateStationLine={updateStationLine}
		                            setIsPopupVisible={value => {
			                            setPopupsOnClick(v => ({
				                            ...v,
				                            removeStation: [onClickTemporary, onClickDefinitive]
			                            }))
			                            setIsPopupVisible(v => ({...v, removeStation: value}))
		                            }}
		                            optionalAttribute={"isSuspended"}/>
	)
}
