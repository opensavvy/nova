/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useState} from 'react'
import StationPopup from "./StationPopup"
import Select from "../../../../Select"

export default function AddStationPopup({
	                                        isPopupVisible,
	                                        setIsPopupVisible,
	                                        popupsOnClick,
	                                        suspendedStations
                                        }) {


	/**
	 * Local state which stores the chosen station number
	 */
	const [chosenStation, setChosenStation] = useState(null)

	/**
	 * Local state which stores the choice between the two options
	 */
	const [radioChoice, setRadioChoice] = useState("")

	/**
	 * Effect used to set the chosen station to the first suspended station, when suspendedStations changes
	 */
	useEffect(() => {
		setChosenStation(suspendedStations?.[0]?.number ?? null)
	}, [suspendedStations])

	const name = "addStation"

	return (
		<StationPopup name={name} popupOpen={isPopupVisible} setPopupOpen={setIsPopupVisible}
		              title={"Type d'installation"} buttons={[
			{
				label: "Valider",
				className: "bg-green-600 hover:bg-green-500 focus:ring-green-500",
				onClick: () => popupsOnClick[name][0](radioChoice, chosenStation)
			}]}>
			<div className="mt-4">
				<div>
					<label className="inline-flex items-center">
						<input type="radio" className="form-radio" name="radio"
						       value="reinstall" onChange={v => setRadioChoice(v.target.value)}
						       checked={radioChoice === "reinstall"}/>
						<span className="ml-2">Réinstaller une station démontée temporairement</span>
					</label>
					{radioChoice === "reinstall" &&
						<Select className={"ml-4 my-4"} onChange={e => setChosenStation(e.target.value)}
						        options={suspendedStations.map(s => (
							        {
								        value: s.number,
								        text: s.number
							        }))}/>}
				</div>
				<div>
					<label className="inline-flex items-center">
						<input type="radio" className="form-radio" name="radio"
						       value="install" onChange={v => setRadioChoice(v.target.value)}
						       checked={radioChoice === "install"}/>
						<span className="ml-2">Installer une nouvelle station</span>
					</label>
				</div>
			</div>
		</StationPopup>
	)
}
