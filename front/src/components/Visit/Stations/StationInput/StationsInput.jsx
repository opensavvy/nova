/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useEffect, useMemo, useState} from "react"
import Table from "../../../Table/Table"
import {StationLineMemo} from "./StationLine"
import {getLastVisitConnectedStations, getPercentages, getUndisplayedStationList} from "../../../../services/stations"
import {useDispatch, useSelector} from "react-redux"
import {
	addStationLine,
	initializeTable,
	installStation,
	removeStationLine,
	resetTable,
	updateStationLine,
	updateUnchangedStationNumber
} from "../stationsSlice"
import StationInputBurger from "./StationInputBurger"
import StationLineAddButton from "./StationLineAddButton"
import {equals} from "../../../../utils/functions"
import {initializeReplicaTable, resetReplicaTable} from "../stationsReplicaSlice"
import AddStationPopup from "./StationPopup/AddStationPopup"
import RemoveStationPopup from "./StationPopup/RemoveStationPopup"

export default function StationsInput({worksite, event_id, type, setIsHistoryVisible, getStatus}) {

	/**
	 * Percentages used in consumption select menu
	 * @type {Number[]}
	 */
	const percentages = useMemo(getPercentages, [])

	/**
	 * Number of stations on the worksite
	 * @type {number}
	 */
	const stationNumber = type === "ssol" ? worksite.ssolNumber : worksite.sbNumber

	/**
	 * Array of stations which reflects the table r an empty array if there is no previous state
	 * @type {Object[]}
	 */
	const stations = useSelector(/**
	 * @param state {{stations: Array}}
	 */
	state => state.stations[event_id] ? state.stations[event_id][type] : [])

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * State which corresponds to the number of newly installed stations
	 */
	const [newStationNumber, setNewStationNumber] = useState(0)

	/**
	 * Boolean state which tells if an added station line's number has been validated by the user
	 */
	const [isLastAddedLineStationNumberChosen, setisLastAddedLineStationNumberChosen] = useState(true)

	/**
	 * Currently suspended stations of a specific type
	 * @type {Array}
	 */
	const suspendedStations = useMemo(() => worksite.stations.filter(s => {
		return s.type.short_name === type && s.is_suspended && stations.filter(st => st.station === `/stations/${s.id}`).every(st => !st.isReinstalled)
	}), [worksite, stations, type])


	/**
	 * Function called to install a station
	 * @type {Function}
	 */
	const addNewStation = useCallback(() => {
		const sortedStations = worksite.stations.filter(s => s.type.short_name === type).sort((s1, s2) => s2.number - s1.number)
		dispatch(installStation({
			event_id,
			type,
			number: (sortedStations?.[0]?.number ?? 0) + newStationNumber + 1
		}))
		setNewStationNumber(c => c + 1)
	}, [stations, newStationNumber])

	/**
	 * Function called to reset the table to its initial state
	 * @type {Function}
	 */
	const resetTableCallback = useCallback(() => {
		const stations = getLastVisitConnectedStations(worksite, type)
		dispatch(resetTable({
			event_id,
			type,
			stations
		}))
		//noinspection JSCheckFunctionSignatures
		dispatch(resetReplicaTable({
			event_id,
			stations: worksite.stations,
			type
		}))
		setNewStationNumber(0)
		setisLastAddedLineStationNumberChosen(true)
	}, [])

	/**
	 * String labels of the table
	 * @type {string[]}
	 */
	const labels = useMemo(() => ["N°", "Connexion", "Cons. préc", "Cons. act", "Activité", "Statut", null], [])

	/**
	 * Local state which stores if popups are visible (a key for each popup type)
	 */
	const [isPopupVisible, setIsPopupVisible] = useState({addStation: false, removeStation: false})

	/**
	 * Local state which stores popup onClick callbacks (a key for each popup type)
	 */
	const [popupsOnClick, setPopupsOnClick] = useState({addStation: [], removeStation: []})

	/**
	 * Complete labels of the table (strings and burger button)
	 */
	const labelsWithBurger = useMemo(() => [...labels,
		<StationInputBurger addNewStation={addNewStation}
		                    resetTable={resetTableCallback}
		                    setIsHistoryVisible={setIsHistoryVisible} setIsPopupVisible={setIsPopupVisible} type={type}
		                    event_id={event_id} worksite={worksite} suspendedStations={suspendedStations}
		                    setPopupsOnClick={setPopupsOnClick}/>], [addNewStation, resetTableCallback, setIsHistoryVisible, setIsPopupVisible, type, event_id, worksite, suspendedStations, setPopupsOnClick])

	/**
	 * State used to store hidden station (ie stations which are not yet displayed but exist on the worksite)
	 */
	const [hiddenStations, setHiddenStations] = useState(getUndisplayedStationList(worksite, type, stations, stationNumber))

	/**
	 * Function called to add a station line to the table
	 * @type {Function}
	 */
	const addLine = useCallback(() => {
		dispatch(updateUnchangedStationNumber({
			event_id,
			type,
			number: hiddenStations[0]?.value
		}))
		if (hiddenStations.length >= 1) {
			dispatch(addStationLine({event_id, type}))
		}

	}, [stations, hiddenStations])

	/**
	 * Effect used to initialize stations state with stations which were connected during the last visit
	 * (only if there is no state in local storage)
	 */
	useEffect(() => {
		const stations = getLastVisitConnectedStations(worksite, type)
		//noinspection JSCheckFunctionSignatures
		dispatch(initializeReplicaTable({event_id, type, stations}))
		dispatch(initializeTable({event_id, type, stations}))
	}, [])

	/**
	 * Effect used to update hidden stations array when stations are modified
	 */
	useEffect(() => {
		const list = getUndisplayedStationList(worksite, type, stations, stationNumber)
		if (equals(list, hiddenStations)) {
			return
		}
		setHiddenStations(list)
	}, [stations])

	/**
	 * Function used to delete a station line
	 * @type {Function}
	 */
	const deleteStationLineCallback = useCallback(payload => {
		dispatch(removeStationLine(payload))
	}, [])

	/**
	 * Function used to update a station line
	 * @type {Function}
	 */
	const updateStationLineCallback = useCallback(payload => dispatch(updateStationLine(payload)), [])

	return (
		<>
			<AddStationPopup isPopupVisible={isPopupVisible} setIsPopupVisible={setIsPopupVisible}
			                 suspendedStations={suspendedStations} popupsOnClick={popupsOnClick}/>

			<RemoveStationPopup setIsPopupVisible={setIsPopupVisible} type={type} event_id={event_id}
			                    popupsOnClick={popupsOnClick} isPopupVisible={isPopupVisible}/>

			<Table className={"rounded-lg"}
			       ths={labelsWithBurger}
			       trs={[stations.map((station, index) => <StationLineMemo labels={labels} station={station}
			                                                               worksite={worksite}
			                                                               hiddenStations={!station.stationNumber ? hiddenStations : null}
			                                                               setisLastAddedLineStationNumberChosen={setisLastAddedLineStationNumberChosen}
			                                                               key={station.stationNumber ? `${station.stationNumber.toString()}${station.indexName ?? ''}` : hiddenStations[0].value.toString()}
			                                                               percentages={percentages}
			                                                               manuallyAdded={station.manuallyAdded}
			                                                               index={index}
			                                                               event_id={event_id}
			                                                               setNewStationNumber={setNewStationNumber}
			                                                               setIsPopupVisible={setIsPopupVisible}
			                                                               setPopupsOnClick={setPopupsOnClick}
			                                                               type={type}
			                                                               getStatus={getStatus}
			                                                               deleteStationLine={deleteStationLineCallback}
			                                                               updateStationLine={updateStationLineCallback}/>),
				       <StationLineAddButton key={"plus"} addLine={addLine}
				                             disabled={!isLastAddedLineStationNumberChosen || hiddenStations.length < 1}/>]}
			/>
		</>
	)
}
