/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useMemo} from "react"
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon"
import {getRestrictedAttributes, getStationValue} from "../../../../services/stations"

export default function StationLineContextMenuItem({
	                                                   icon,
	                                                   event_id,
	                                                   type,
	                                                   index,
	                                                   className,
	                                                   station,
	                                                   attributes,
	                                                   attribute,
	                                                   optionalAttribute,
	                                                   excludedAttributes,
	                                                   label,
	                                                   cancelLabel,
	                                                   unitAction,
	                                                   updateStationLine,
	                                                   disabledCondition,
	                                                   setIsPopupVisible
                                                   }) {

	/**
	 * Station status attributes which are not equal to this line's attribute
	 */
	const restrictedAttributes = useMemo(() => {
		return getRestrictedAttributes(attributes, attribute, excludedAttributes)
	}, [attributes, attribute, excludedAttributes])

	/**
	 * Function that gets the condition which disables the context menu item.
	 * Returns true (=== disabled the item) if at least one of restrictedAttributes are true for the specific station AND optional attribute does not exist or is false for the station
	 * @type {Function}
	 */
	const getCondition = useCallback(station => {
		return restrictedAttributes.reduce((prev, curr) => {
			return prev || station[curr]
		}, false) && (!optionalAttribute || !station[optionalAttribute])
	}, [restrictedAttributes])


	return (
		<ContextMenuItemWithIcon icon={icon}
		                         disabled={disabledCondition ?? getCondition(station)}
		                         onClick={() => {
			                         if (setIsPopupVisible && !station[attribute]) {
				                         setIsPopupVisible(true)
			                         } else {
				                         updateStationLine({
					                         event_id: event_id,
					                         type: type,
					                         index: index,
					                         value: getStationValue(restrictedAttributes, station, [attribute], cancelLabel === null),
					                         unitAction: unitAction
				                         })
			                         }
		                         }}
		                         className={className}>
			<div className={"font-semibold"}>{station[attribute] ? cancelLabel ?? label : label}</div>
		</ContextMenuItemWithIcon>
	)
}
