/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {createSlice} from "@reduxjs/toolkit"
import {addToObservations} from "../../../services/observations"


export const observationsSlice = createSlice({
	name: "observations",
	initialState: {},
	reducers: {
		modifyObservation: (state, action) => {
			return {
				...state,
				[action.payload.event_id]: {
					...state[action.payload.event_id],
					[action.payload.type]: action.payload.value
				}
			}
		},
		addToObservation: (state, action) => {
			const currentValue = state[action.payload.event_id]?.[action.payload.type]
			if (!currentValue || currentValue && !currentValue.includes(action.payload.value)) {
				return {
					...state,
					[action.payload.event_id]: {
						...state[action.payload.event_id],
						[action.payload.type]: addToObservations(currentValue ?? "", action.payload.value)
					}
				}
			}
		},
		removeObservationData: (state, action) => {
			delete state[action.payload.event_id]
		}
	}
})

export const {modifyObservation, removeObservationData, addToObservation} = observationsSlice.actions

export default observationsSlice.reducer
