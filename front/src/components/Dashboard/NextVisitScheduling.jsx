/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import TextBadge from "../TextBadge"
import {getFormattedDateAndTime, getFormattedDuration} from "../../utils/functions"

export default function NextVisitScheduling({nextVisit}) {
	return (
		nextVisit ?
			<div className={"flex w-full justify-between items-baseline"}>
				<div className={"text-gray-700 mb-2"}>
					<TextBadge>
						{getFormattedDateAndTime(nextVisit.scheduled_at)}
					</TextBadge>
				</div>
				<div className={"text-gray-700 text-xs mb-2"}>
					<TextBadge color={"blue"} small={true}>
						{getFormattedDuration(nextVisit.scheduled_duration)}
					</TextBadge>
				</div>
			</div> :
			<div className={"flex"}>
				<TextBadge color={"red"}>Aucun RDV planifié et assigné</TextBadge>
			</div>
	)
}
