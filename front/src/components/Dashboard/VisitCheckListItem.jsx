/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import RoundTick from "../../Icons/RoundTick"
import {getFormattedDuration} from "../../utils/functions"

export default function VisitCheckListItem({step_name, step_duration, step_done}) {
	return (
		<li className="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800"
		    key={step_name}>
			<div className={"flex items-center justify-start text-sm " + (step_done && "line-through text-gray-400")}>
				<span className={"text-left"}>
					{step_name}
				</span>
				<span
					className={"mx-4 flex items-center text-gray-400 dark:text-gray-300 min-w-max " + (step_done && "line-through")}>
					{step_duration ? getFormattedDuration(step_duration) : "0 min"}
				</span>
			</div>
			<div>
				<RoundTick checked={step_done}/>
			</div>
		</li>
	)
}
