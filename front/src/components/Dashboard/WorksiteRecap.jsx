/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'
import Link from "../../Icons/Link"
import Visit from "../../Icons/Visit"
import SSOL from "../../Icons/SSOL"
import SB from "../../Icons/SB"
import WorksiteRecapItem from "./WorksiteRecapItem"
import {getFormattedDate} from "../../utils/functions"

export default function WorksiteRecap({worksite, setNotesOpen}) {
	worksite = worksite.worksite ? worksite.worksite : worksite
	return (
		<div
			className="shadow-lg rounded-xl bg-blue-500 w-full p-6 dark:bg-gray-800 relative overflow-hidden flex flex-col max-h-52">
			<div className={"flex w-full mb-4 justify-between"}>
				<p className="text-white text-xl">
					Récapitulatif
				</p>
				{worksite.notesNumber > 0 &&
					<div className={"flex flex-row"}>
						<button type="button" className="w-8 h-8 text-base  rounded-full text-white bg-red-400 ml-2"
						        onClick={() => setNotesOpen(true)}>
							<span className="p-1">{worksite.notesNumber}</span>
						</button>
					</div>
				}
			</div>
			<div className={"flex flex-col flex-wrap w-full max-h-44"}>
				<WorksiteRecapItem value={getFormattedDate(worksite.installation_date)}
				                   label={"Installation"}><Link/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.visitNumber} label={"Visites"}><Visit/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.ssolNumber} label={"SSOL"}><SSOL/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.sbNumber} label={"SB"}><SB/></WorksiteRecapItem>
			</div>
		</div>
	)
}
