/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useState} from 'react'
import WorksiteIdentification from "./Dashboard/WorksiteIdentification"
import WorksiteRecap from "./Dashboard/WorksiteRecap"
import VisitCheckList from "./Dashboard/VisitCheckList"
import CrossButton from "./CrossButton"
import {WorksiteMenu} from "./Dashboard/WorksiteMenu"
import Notes from "./Dashboard/Notes"
import RightChevron from "../Icons/RightChevron"
import IconButton from "./IconButton"
import CardContainer from "./Dashboard/CardContainer"
import DashboardItemIcon from "./Dashboard/DashboardItemIcon"


export default function DashboardItem({worksite, isVisit = false}) {
	const [buttonClicked, setButtonClicked] = useState(false)
	const [notesOpen, setNotesOpen] = useState(false)
	const worksiteDetails = worksite.worksite ? worksite.worksite : worksite

	return (
		<div className="container flex flex-col mx-auto w-full items-center justify-center sidebar-content">
			<ul className="flex flex-col w-full h-full">
				<li key={worksite.code?.toString() || worksite.worksite.code.toString()}
				    className={`border-gray-400 flex flex-row ${!isVisit ? 'mb-4' : 'h-full'} `}>
					<div
						className={`shadow bg-white ${!isVisit && worksite.visit?.isCompleted ? 'opacity-70' : ''} relative dark:bg-gray-800 rounded-md flex flex-1 items-center flex-col h-full w-full ${!isVisit ? 'lg:flex-row transform hover:-translate-y-1 hover:shadow-lg ease-in-out transition duration-500 p-4' : 'p-6'}`}>
						{!isVisit && worksite.visit?.isCompleted && <DashboardItemIcon type={"done"}/>}
						{!isVisit && worksite.visit?.isPending && <DashboardItemIcon type={"pending"}/>}
						<CardContainer isVisit={isVisit} className={"lg:max-w-xs"}>
							<WorksiteIdentification worksite={worksiteDetails}/>
						</CardContainer>

						<CardContainer isVisit={isVisit} hideCondition={buttonClicked || notesOpen}>
							<WorksiteRecap worksite={worksite} setNotesOpen={setNotesOpen}/>
						</CardContainer>

						<CardContainer isVisit={isVisit} hideCondition={notesOpen}>
							<VisitCheckList worksite={worksite}/>
						</CardContainer>

						{notesOpen &&
							<CardContainer isVisit={isVisit} className={buttonClicked ? 'max-w-2xl' : 'max-w-3xl'}>
								<Notes setNotesOpen={setNotesOpen} worksite={worksiteDetails}/>
							</CardContainer>

						}
						{buttonClicked &&
							<CardContainer isVisit={isVisit}>
								<WorksiteMenu worksite={worksite}/>
							</CardContainer>
						}

						{!isVisit && <div className={"flex self-stretch justify-center"}>
							{buttonClicked ? <CrossButton onClick={() => setButtonClicked(false)}/> :
								<IconButton icon={<RightChevron width={12} height={12}/>}
								            onClick={() => setButtonClicked(true)}/>

							}
						</div>}
					</div>
				</li>
			</ul>
		</div>
	)
}

