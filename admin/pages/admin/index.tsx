import Head from "next/head";

import configData from './config.json'

const AdminLoader = () => {
  if (typeof window !== "undefined") {
      const hostname = window.location.hostname
      const baseURL : string = configData[hostname].API_BASE_URL
      const { HydraAdmin } = require("@api-platform/admin");
    return <HydraAdmin entrypoint={baseURL} />;
  }

  return <></>;
};

const Admin = () => (
  <>
    <Head>
      <title>API Platform Admin</title>
    </Head>

    <AdminLoader />
  </>
);
export default Admin;
