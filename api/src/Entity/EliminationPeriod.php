<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\EliminationPeriodRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource]
#[ORM\Entity(repositoryClass: EliminationPeriodRepository::class)]
class EliminationPeriod
{
	#[Groups('worksite_summary')]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[ORM\OneToOne(inversedBy: 'startingEliminationPeriod', targetEntity: Visit::class, cascade: ['persist', 'remove'])]
	#[ORM\JoinColumn(nullable: false)]
	private $startVisit;

	#[ORM\OneToOne(inversedBy: 'endingEliminationPeriod', targetEntity: Visit::class, cascade: ['persist', 'remove'])]
	private $endVisit;

	#[ORM\ManyToOne(targetEntity: Worksite::class, inversedBy: 'eliminationPeriods')]
	#[ORM\JoinColumn(nullable: false)]
	private $worksite;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getStartVisit(): ?Visit
	{
		return $this->startVisit;
	}

	public function setStartVisit(Visit $startVisit): self
	{
		$this->startVisit = $startVisit;

		return $this;
	}

	public function getEndVisit(): ?Visit
	{
		return $this->endVisit;
	}

	public function setEndVisit(?Visit $endVisit): self
	{
		$this->endVisit = $endVisit;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}
}
