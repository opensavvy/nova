<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;


use App\Entity\StationStatus;
use App\Entity\Visit;
use Closure;
use Doctrine\Common\Collections\Collection;

class VisitReportStationService
{

	/** Generates station info array with specific information about the stations
	 * @param Visit $visit
	 * @return array
	 */
	public function getStationInfos(Visit $visit): array
	{
		$statuses = $visit->getStationStatuses();
		return [
			"connectedSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getIsConnected()),
			"consummatingSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getConsumption() > 0),
			"connectedSB" => $this->getStations($statuses, "sb", fn(StationStatus $s) => $s->getIsConnected()),
			"consummatingSB" => $this->getStations($statuses, "sb", fn(StationStatus $s) => $s->getConsumption() > 0),
			"rechargedSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getIsRecharged()),
			"rechargedSB" => $this->getStations($statuses, "sb", fn(StationStatus $s) => $s->getIsRecharged()),
			"lidChangedSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getIsLidChanged()),
			"replacedSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getIsReplaced()),
			"removedSB" => $this->getStations($statuses, "sb", fn(StationStatus $s) => $s->getIsRemoved()),
			"notAccessibleSSOL" => $this->getStations($statuses, "ssol", fn(StationStatus $s) => $s->getIsNotAccessible()),
			"notAccessibleSB" => $this->getStations($statuses, "sb", fn(StationStatus $s) => $s->getIsNotAccessible()),
		];
	}

	/**
	 * Gets a string which represents the set of station numbers which corresponds to the type and the callback
	 * @param Collection $statuses Station statuses
	 * @param String $type Type of the station (ssol, sb)
	 * @param Closure $fn Callback
	 * @return string
	 */
	private function getStations(Collection $statuses, string $type, Closure $fn): string
	{
		$collection = $statuses->filter(fn(StationStatus $s) => $s->getStation()->getType()->getShortName() === $type && $fn($s))->map(fn(StationStatus $s) => $s->getStation()->getNumber());
		return join(",", $collection->toArray());
	}
}
