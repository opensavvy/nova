<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\EliminationPeriod;
use App\Entity\Import\PDFStation;
use App\Entity\Import\PDFVisitInfo;
use App\Entity\Note;
use App\Entity\Station;
use App\Entity\StationType;
use App\Entity\Visit;
use App\Entity\VisitType;
use App\Entity\Worksite;
use App\Entity\WorksiteStatus;
use App\Service\Import\PDFImportService;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Google\Service\Calendar\Event;

class WorksiteImportService
{
	/**
	 * @var string URL of export script in old version of Nova
	 */
	private string $importDataURL = "https://sos-termites.com/clients/export.php?code=";

	/**
	 * @var string URL of export script in old version of Nova
	 */
	private string $importReportsURL = "https://sos-termites.com/clients/get_list_compte_rendus.php?code=";

	/**
	 * @var string Location of downloaded PDF reports
	 */
	private string $pdfPath = "./pdf/";

	public function __construct(private GoogleCalendarService $calendarService, private GoogleDriveService $driveService, private EntityManagerInterface $entityManager, private TypesenseService $typesenseService, private PDFImportService $PDFImportService)
	{

	}

	/**
	 * @param string $code Worksite's code
	 * @throws \Http\Client\Exception | Exception
	 * Import the specified worksite
	 */
	public function importWorksite(string $code)
	{
		$worksite = $this->createWorksite($code);
		$this->entityManager->persist($worksite);
		$this->entityManager->flush();
		$this->typesenseService->upsertWorksite($worksite);

	}

	/**
	 * @param $code Worksite's code
	 * @return Worksite
	 * @throws Exception
	 * Creates a worksite
	 */
	private function createWorksite($code): Worksite
	{
		$data = $this->getData($code);
		$address = (new Address())
			->setStreet($data["adresse"])
			->setPostalCode($data["cp"])
			->setCity($data["ville"]);
		$contacts = $this->createContacts($data);
		$stations = $this->createStations($data);
		$notes = $this->createNotes($data);
		$visits = $this->createVisits($data);
		[$eliminationPeriods, $newVisits] = $this->createEliminationPeriods($data, $visits);
		$visits = [...$visits, ...$newVisits];
		$worksite = new Worksite();
		$pdfReports = $this->downloadReportsFromDrive($code);
		foreach ($contacts as $contact) {
			$worksite->addContact($contact);
		}
		foreach ($stations as $station) {
			$worksite->addStation($station);
		}
		foreach ($notes as $note) {
			$worksite->addNote($note);
		}
		foreach ($eliminationPeriods as $eliminationPeriod) {
			$worksite->addEliminationPeriod($eliminationPeriod);
		}
		foreach ($visits as $visit) {
			if ($visit instanceof Visit && $visit->getType() && $visit->getType()->getShortName() !== "installation") {
				$infos = $this->getPDFInformation($code, $visit);
				if ($infos["stations"]) {
					$this->addPDFStationsToVisit($infos["stations"], $stations, $visit);
				}
				if ($infos["visit"]) {
					$this->addVisitInfo($infos["visit"], $visit, $worksite);
				}
				$this->setReportsFileID($visit, $pdfReports);
			}
			$worksite->addVisit($visit);
		}
		return $worksite
			->setAddress($address)
			->setCode($code)
			->setName($data["nom_client"])
			->setStatus($this->getWorksiteStatus($data))
			->setNextVisitType($this->getVisiteType($data["type_prochaine_visite"]))
			->setInstallationDate(DateTime::createFromFormat('d/m/Y', $data["date_installation"]))
			->setNextVisitLimitDate(DateTime::createFromFormat('d/m/Y', $data["proch_visite"]))
			->setProtectedBuiltSurface(intval($data["bti_protg_m"]))
			->setProtectedBuiltPerimeter(intval($data["bti_protg_ml"]))
			->setProtectedUndevelopedSurface(intval($data["non_bti_protg_m"]))
			->setProtectedIndevelopedPerimeter(intval($data["non_bti_protg_ml"]))
			->setBuiltControlDuration(StepDurationCalculationService::recalculate(DateInterval::createFromDateString($data["dureetheocb"] . "min")))
			->setVisitPerYear(intval($data["nb_visites_par_an"]))
			->setAnnualVisitDate(DateTime::createFromFormat('d/m/Y', $data["date_installation"]));
	}

	/**
	 * @param $code Worksite's code
	 * @return array Data
	 * Gets data from server
	 */
	private function getData($code): array
	{
		return json_decode(file_get_contents($this->importDataURL . $code), true);
	}

	/**
	 * @param array $data Data
	 * @return array
	 * Creates all contacts
	 */
	private function createContacts(array $data): array
	{
		$contacts = [];
		$contacts[] = $this->createContact($data["gsm_precision"], $data["gsm"], $data["adresse_email_1"]);
		for ($i = 2; $i <= 3; $i++) {
			if (isset($data["gsm$i"]) && $data["gsm$i"] !== "") {
				$contacts[] = $this->createContact($data["gsm$i" . "_precision"], $data["gsm$i"], $data["adresse_email_$i"] ?? null);
			}
		}
		return $contacts;
	}

	/**
	 * @param $name Contact's name
	 * @param $phone_number Contact's phone number
	 * @param $email Contact's email
	 * @return Contact
	 * Creates a contact from information
	 */
	private function createContact($name, $phone_number, $email): Contact
	{
		return (new Contact())
			->setName($name)
			->setEmailAddress($email)
			->setPhoneNumber($phone_number);
	}

	/**
	 * @param $data
	 * @return array
	 * Creates all stations of a worksite
	 */
	private function createStations($data): array
	{
		$stations = [];
		$stationTypeRepository = $this->entityManager->getRepository(StationType::class);
		$ssolType = $stationTypeRepository->findOneBy(["short_name" => "ssol"]);
		$sbType = $stationTypeRepository->findOneBy(["short_name" => "sb"]);
		if ($ssolType instanceof StationType && $sbType instanceof StationType) {
			$ssolNumber = intval($data["nb_sshd_installes"]);
			$sbNumber = intval($data["nb_sb_installes"]);
			for ($i = 1; $i <= $ssolNumber; $i++) {
				$stations[] = $this->createStation($ssolType, $i);
			}
			for ($i = 1; $i <= $sbNumber; $i++) {
				$stations[] = $this->createStation($sbType, $i);
			}
		}
		return $stations;
	}

	/**
	 * @param StationType $type Type of the station
	 * @param $number Number of the station
	 * @return Station
	 * Creates a station
	 */
	private function createStation(StationType $type, $number): Station
	{
		return (new Station())
			->setType($type)
			->setNumber($number)
			->setIsInstalled(true)
			->setIsSuspended(false);
	}

	/**
	 * @param array $data Data
	 * @return array
	 * Creates all notes of a worksite
	 */
	private function createNotes(array $data): array
	{
		$notes = [];
		$recurrent = $data["notes_tech_recurrent"];
		$ponctuel = $data["notes_tech_ponctuel"];
		foreach (explode("\r\n", $recurrent) as $rec_content) {
			if ($note = $this->createNote($rec_content, false)) {
				$notes[] = $note;
			}
		}
		foreach (explode("\r\n", $ponctuel) as $ponc_content) {
			if ($note = $this->createNote($ponc_content, true)) {
				$notes[] = $note;
			}
		}
		return $notes;
	}

	/**
	 * @param $content Note's content
	 * @param $isPonctual Boolean Boolean indicating if the note is punctual
	 * @return Note|null
	 * Creates a note
	 */
	private function createNote($content, bool $isPonctual): ?Note
	{
		if ($content !== "") {
			return (new Note())
				->setContent($content)
				->setIsVisible(true)
				->setIsPunctual($isPonctual);
		}
		return null;
	}

	/**
	 * Creates all visits of a worksite
	 * @param array $data Data
	 * @return ArrayCollection
	 * @throws Exception
	 */
	private function createVisits(array $data): ArrayCollection
	{
		$createdVisits = [];
		$visits = explode("\n", $data["historique_visites"]);
		foreach ($visits as $visit) {
			if ($visit !== "") {
				$parts = array_map(fn($p) => trim($p), explode(":", $visit));
				$date = DateTime::createFromFormat('d-m-Y', $parts[0]);
				$type = $this->getVisitType($parts[1]);
				if ($date instanceof DateTime && $type instanceof VisitType) {
					$createdVisits = [...$createdVisits, ...$this->getVisits($data["nickname"], $date, $type)];
				}
			}
		}
		return new ArrayCollection($createdVisits);
	}

	/**
	 * @param string $long_name
	 * @return VisitType|null
	 * Gets visit type from string
	 */
	private function getVisitType(string $long_name): ?VisitType
	{
		$correspondance = [
			"Installation" => 1,
			"Visite annuelle surveillance (SSOL + CB)" => 2,
			"Visite annuelle surveillance (SSOL+CB)" => 2,
			"visite-1-surveillance" => 3,
			"Visite 1 Surveillance" => 3,
			"Visite contrôle complémentaire si SSOL connectés" => 4,
			"Visite contrôle SSOL + SB" => 5,
			"Visite contrôle SSOL+SB" => 5,
			"Visite contrôle intermédiaire SB uniquement" => 6,
			"Visite contrôle annuel SSOL + SB + CB" => 7,
			"Visite contrôle annuel SSOL+SB+CB" => 7,
			"Visite Hors Cadre" => 8
		];
		$visitType = $this->entityManager->getRepository(VisitType::class)->find($correspondance[$long_name]);
		if ($visitType instanceof VisitType) {
			return $visitType;
		}
		return null;
	}

	/**
	 * @param string $code Worksite's code
	 * @param DateTime $date Date during which we want to get visits
	 * @param VisitType $type Type of the visit
	 * @return array
	 * @throws Exception
	 * Gets all visits of a worksite
	 */
	private function getVisits(string $code, DateTime $date, VisitType $type): array
	{
		$visits = [];
		/**
		 * @var $events Event[]
		 */
		$event = $this->calendarService->getWorksiteEvents($code, $date)->first();
		if ($event instanceof Event) {
			$visits[] = $this->createVisit($event, $type);
		}
		return $visits;
	}

	/**
	 * @param Event $event Event which corresponds to the visit
	 * @param VisitType $type Type of the visit
	 * @return Visit
	 * @throws Exception
	 * Creates a visit
	 */
	private function createVisit(Event $event, VisitType $type): Visit
	{
		$begin = new DateTime($event->getStart()->getDateTime());
		$end = new DateTime($event->getEnd()->getDateTime());
		return (new Visit())
			->setScheduledAt($begin)
			->setEstimatedDuration($end->diff($begin))
			->setEffectiveDuration($end->diff($begin))
			->setEventId($event->getId())
			->setCreatedAt($begin)
			->setUpdatedAt($begin)
			->setIsCompleted(true)
			->setIsPending(false)
			->setType($type);
	}

	/**
	 * Creates elimination periods
	 * Creates visits if an elimination period corresponds to a non-existing visit.
	 * @param array $data PDF Data
	 * @param ArrayCollection $visits Visit collection
	 * @return array[]
	 */
	private function createEliminationPeriods(array $data, ArrayCollection $visits): array
	{
		$periods = [];
		$newVisits = [];
		$begin = $this->getFormattedDates($data["anciens_curatifs"], $data["date_deb_curatif"]);
		$end = $this->getFormattedDates($data["anciennes_eliminations"], $data["date_elim"]);
		foreach ($begin as $index => $beginDate) {
			$beginVisit = $this->findVisitFromDate($visits, $beginDate);
			if (!$beginVisit) {
				$beginVisit = $this->getStandaloneVisit($beginDate);
				$newVisits[] = $beginVisit;
			}
			$endDate = $end[$index] ?? null;
			if ($endDate) {
				$endVisit = $this->findVisitFromDate($visits, $endDate);
				if (!$endVisit) {
					$endVisit = $this->getStandaloneVisit($endDate);
					$newVisits[] = $endVisit;
				}
			} else {
				$endVisit = null;
			}
			$periods[] = (new EliminationPeriod())
				->setStartVisit($beginVisit)
				->setEndVisit($endVisit);
		}
		return [$periods, $newVisits];
	}

	/**
	 * Gets formatted dates from comma-separated date lists ("old" and "current" dates)
	 * Accepts "-" and "/" separators, removes duplicates and sorts chronologically.
	 * @param string $old "Old" dates
	 * @param string $current "Current" dates
	 * @return array
	 */
	private function getFormattedDates(string $old, string $current): array
	{
		$oldArray = explode(',', $old);
		$currentArray = explode(',', $current);
		$formattedDates = array_map(fn($d) => DateTime::createFromFormat('d/m/Y', $d)->setTime(0, 0), array_filter(array_unique(array_map(fn($date) => str_replace("-", "/", $date), array_merge($oldArray, $currentArray))), fn($da) => $da !== ""));
		sort($formattedDates);
		return $formattedDates;
	}

	/**
	 * Finds the visit which corresponds to the specified date
	 * @param ArrayCollection $visits Collection of visits
	 * @param DateTime $date Date
	 * @return false|mixed
	 */
	private function findVisitFromDate(ArrayCollection $visits, DateTime $date): Visit|false
	{
		return $visits->filter(fn(Visit $v) => $v->getScheduledAt()->format('d/m/Y') === $date->format('d/m/Y'))->first();
	}

	/**
	 * Creates a visit which is independent from data import
	 * @param DateTime $date Date of the visit
	 * @return Visit
	 */
	private function getStandaloneVisit(DateTime $date): Visit
	{
		return (new Visit())
			->setScheduledAt($date)
			->setCreatedAt($date)
			->setUpdatedAt($date)
			->setIsPending(false)
			->setIsCompleted(true)
			->setEventId("")
			->setEstimatedDuration(DateInterval::createFromDateString('0 min'))
			->setEffectiveDuration(DateInterval::createFromDateString('0 min'));
	}

	/**
	 * Downloads reports from Google Drive if necessary (client and confidential)
	 * @param $code Worksite's code
	 * @return array
	 */
	private function downloadReportsFromDrive($code): array
	{
		$list = array_filter($this->getReportsList($code), fn($form) => $form["form"] === "v");
		$path = $this->pdfPath . $code;
		if (!file_exists($path)) {
			mkdir($path);
			mkdir($path . '/client/');
			mkdir($path . '/conf/');
		} else {
			return $list;
		}
		array_map(function ($form, $filename) use ($path) {
			$clientFile = $this->driveService->downloadFile($form["fileid"]);
			/** @noinspection PhpUndefinedMethodInspection */
			file_put_contents($path . "/client/$filename", $clientFile->getBody());
			if (isset($form["conf"])) {
				$confFile = $this->driveService->downloadFile($form["conf"]);
				/** @noinspection PhpUndefinedMethodInspection */
				file_put_contents($path . "/conf/$filename", $confFile->getBody());
			}
		}, $list, array_keys($list));
		return $list;
	}

	/**
	 * @param $code Worksite's code
	 * @return array Data
	 * Gets reports list from server
	 */
	private function getReportsList($code): array
	{
		return json_decode(file_get_contents($this->importReportsURL . $code), true)["chantier"];
	}

	/**
	 * Gets information array from Visit entity using pdf report
	 * @param $code Worksite's code
	 * @param Visit $visit The specified Visit
	 * @return array|null
	 */
	private function getPDFInformation($code, Visit $visit): ?array
	{
		$date = $visit->getScheduledAt()->format('d-m-Y');
		$filename = "Visite$date.pdf";
		return $this->PDFImportService->readFromPDF($this->pdfPath . $code . "/conf/$filename", $this->pdfPath . $code . "/client/$filename", $visit);
	}

	/**
	 * Processes PDFStation array to add corresponding stationStatuses to the specified Visit.
	 * @param PDFStation[] $PDFStations PDFStation array
	 * @param Station[] $installedStations Currently installed stations
	 * @param Visit $visit Visit entity
	 */
	private function addPDFStationsToVisit(array $PDFStations, array $installedStations, Visit $visit)
	{
		foreach ($PDFStations as $station) {
			$installedStationArray = array_filter($installedStations, fn($st) => $st->getNumber() === $station->getNumber() && $st->getType()->getShortName() === $station->getType()->getShortName());
			$index = array_key_first($installedStationArray);
			if ($index){
				$installedStation = $installedStationArray[$index];
				if ($installationVisit = $station->getInstalledDuringVisit()) {
					$installedStation->setInstalledDuringVisit($installationVisit);
				}
				$visit->addStationStatus($station->getStationStatus()->setStation($installedStation));
			}
		}
	}

	/**
	 * Adds visit info to visit using provider PDFVisitInfo entity.
	 * @param PDFVisitInfo $visitInfo PDFVisitInfo entity
	 * @param Visit $visit Specified visit
	 * @param Worksite $worksite Specified Worksite
	 */
	private function addVisitInfo(PDFVisitInfo $visitInfo, Visit $visit, Worksite $worksite)
	{
		if ($visitInfo->isEliminationBegin()) {
			if (!$this->getEliminationPeriod($visit, $worksite)) {
				$eliminationPeriod = (new EliminationPeriod())
					->setStartVisit($visit);
				$worksite->addEliminationPeriod($eliminationPeriod);
			}
		} else if ($visitInfo->isEliminationEnd()) {
			$eliminationPeriod = $this->getUnclosedEliminationPeriod($worksite);
			if ($eliminationPeriod instanceof EliminationPeriod && !$this->getEliminationPeriod($visit, $worksite)) {
				$eliminationPeriod->setEndVisit($visit);
			}
		}
		$visit->setDiverseObservation($visitInfo->getDiverseObservations());
		$visit->setInternObservation($visitInfo->getInternObservations());
		$visit->setEffectiveDuration($visitInfo->getEffectiveDuration());
	}

	/**
	 * Gets elimination periods corresponding to a speicifc visit and worksite
	 * @param Visit $visit The visit
	 * @param Worksite $worksite The worksite
	 * @return EliminationPeriod|false
	 */
	private function getEliminationPeriod(Visit $visit, Worksite $worksite): EliminationPeriod|false
	{
		return $worksite->getEliminationPeriods()->filter(fn(EliminationPeriod $p) => $p->getStartVisit() === $visit || $p->getEndVisit() === $visit)->first();
	}

	/**
	 * Gets unclosed elimination period
	 * @param Worksite $worksite Corresponding worksite
	 * @return EliminationPeriod|bool
	 */
	private function getUnclosedEliminationPeriod(Worksite $worksite): EliminationPeriod|bool
	{
		return $worksite->getEliminationPeriods()->filter(fn(EliminationPeriod $p) => $p->getEndVisit() === null)->first();
	}

	/**
	 * Sets PDF reports IDs
	 * @param Visit $visit Specified visit
	 * @param array $pdfReports Array of pdf reports
	 */
	private function setReportsFileID(Visit $visit, array $pdfReports)
	{
		$date = $visit->getScheduledAt()->format('d-m-Y');
		$filename = "Visite$date.pdf";
		if (isset($pdfReports[$filename])) {
			$visit->setClientReportFileID($pdfReports[$filename]["fileid"] ?? null)
				->setInternReportFileID($pdfReports[$filename]["conf"] ?? null);
		}
	}

	/**
	 * @param array $data
	 * @return WorksiteStatus|null Worksite's current status
	 * Get the current status of the Worksite
	 */
	private function getWorksiteStatus(array $data): ?WorksiteStatus
	{
		$worksiteStatus = $this->entityManager->getRepository(WorksiteStatus::class)->findOneBy(["short_name" => $data["status"]]);
		if ($worksiteStatus instanceof WorksiteStatus) {
			return $worksiteStatus;
		}
		return null;
	}

	/**
	 * @param string $type Visit type as a string
	 * @return VisitType|null
	 * Get the visit type from visit type string
	 */
	private function getVisiteType(string $type): ?VisitType
	{
		$visitType = $this->entityManager->getRepository(VisitType::class)->findOneBy(["short_name" => $type]);
		if ($visitType instanceof VisitType) {
			return $visitType;
		}
		return null;
	}
}

