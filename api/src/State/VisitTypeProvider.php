<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\VisitType;
use App\Entity\Worksite;
use App\Repository\VisitTypeRepository;
use App\Service\StepDurationCalculationService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

readonly class VisitTypeProvider implements ProviderInterface
{
	public function __construct(private ProviderInterface $itemProvider, private StepDurationCalculationService $durationCalculationService, private EntityManagerInterface $entityManager)
	{

	}

	/**
	 * Finds corresponding visit types and hydrates them with visit step durations when worksite_id exists
	 * @param Operation $operation
	 * @param array $uriVariables
	 * @param array $context
	 * @return object|array|null
	 */
	public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
	{
		if (isset($context["filters"])){
			$repository = $this->entityManager->getRepository(VisitType::class);
			if ($repository instanceof VisitTypeRepository) {
				$collection = $repository->findByWorksiteStatusAndInstallationOnly($context["filters"]["worksite_status.short_name"], $context["filters"]["is_allowed_from_installation_only"]);

				if (isset($context["filters"]["worksite_id"])) {
					$worksite = $this->entityManager->getRepository(Worksite::class)->find($context["filters"]["worksite_id"]);
					if ($worksite instanceof Worksite) {
						return array_map(function ($v) use ($worksite) {
							if ($v instanceof VisitType) {
								return $this->durationCalculationService->calculateStepsDuration($v, $worksite, DateInterval::createFromDateString("0 min"));
							}
							return null;
						}, $collection);
					}
				}
				return $collection;
			}
		}

		return $this->itemProvider->provide($operation, $uriVariables, $context);
	}
}
