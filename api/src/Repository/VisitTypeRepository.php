<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\VisitType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VisitType|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitType|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitType[]    findAll()
 * @method VisitType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitTypeRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, VisitType::class);
	}

	/**
	 * Finds the visit types which correspond to a specific worksite status and a specific installation only flag
	 * @return VisitType[] Returns an array of VisitType objects
	 */
	public function findByWorksiteStatusAndInstallationOnly(string $worksiteStatus, string $installationOnly): array
	{
		$query = $this->createQueryBuilder('v')
			->innerJoin('v.worksite_status', 's', Join::WITH, 'v.worksite_status = s.id')
			->andWhere('v.is_allowed_from_installation_only = :installationOnly')
			->andWhere('s.short_name = :worksiteStatus')
			->setParameter('installationOnly', $installationOnly)
			->setParameter('worksiteStatus', $worksiteStatus)
			->getQuery();
		return $query->getResult();
	}

	/*
	public function findOneBySomeField($value): ?VisitType
	{
		return $this->createQueryBuilder('v')
			->andWhere('v.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
