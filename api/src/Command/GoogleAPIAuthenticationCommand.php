<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Command;


use App\Service\GoogleAbstractService;
use Exception;
use Google_Client;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

#[AsCommand(name: 'googleapi:authenticate')]
class GoogleAPIAuthenticationCommand extends Command
{
	public function __construct(
		private readonly string $calendarClientID,
		private readonly string $calendarClientSecret,
		private readonly string $calendarTokenPath,
		private readonly string $calendarScope,
		private readonly string $driveClientID,
		private readonly string $driveClientSecret,
		private readonly string $driveTokenPath,
		private readonly string $driveScope,
		string                  $name = null)
	{
		parent::__construct($name);
	}

	protected function configure(): void
	{
	}

	/**
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$helper = $this->getHelper('question');
		$question = new Question('Name of Google API (calendar or drive) ? ');
		$apiType = $helper->ask($input, $output, $question);

		return $this->authenticate($apiType);
	}

	/**
	 * @throws Exception
	 */
	private function authenticate($type): int
	{
		if ($type === "calendar") {
			$client = GoogleAbstractService::configureClient(new Google_Client(), $this->calendarClientID, $this->calendarClientSecret, $this->calendarScope);
			$tokenPath = $this->calendarTokenPath;
		} else if ($type === 'drive') {
			$client = GoogleAbstractService::configureClient(new Google_Client(), $this->driveClientID, $this->driveClientSecret, $this->driveScope);
			$tokenPath = $this->driveTokenPath;
		} else {
			return Command::FAILURE;
		}
		$authUrl = $client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $authUrl);
		print 'Enter verification code: ';
		$authCode = trim(fgets(STDIN));

		// Exchange authorization code for an access token.
		$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
		$client->setAccessToken($accessToken);

		// Check to see if there was an error.
		if (array_key_exists('error', $accessToken)) {
			throw new Exception(join(', ', $accessToken));
		}

		GoogleAbstractService::saveToken($tokenPath, $client);
		return Command::SUCCESS;
	}
}

