<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Serializer;

use App\Entity\User;
use App\Entity\UserOwnedInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UserOwnedDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
	use DenormalizerAwareTrait;

	private const ALREADY_CALLED_DENORMALIZER = "UserOwnedDenormalizerCalled";

	public function __construct(private readonly Security $security)
	{
	}

	/**
	 * @throws ReflectionException
	 */
	public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
	{
		$reflectionClass = new ReflectionClass($type);
		$alreadyCalled = $context[self::ALREADY_CALLED_DENORMALIZER] ?? false;
		return $reflectionClass->implementsInterface(UserOwnedInterface::class) && !$alreadyCalled;
	}

	/**
	 * @throws ExceptionInterface
	 */
	public function denormalize(mixed $data, string $type, string $format = null, array $context = []) : UserOwnedInterface
	{
		$context[self::ALREADY_CALLED_DENORMALIZER] = true;
		/** @var UserOwnedInterface $obj */
		$obj = $this->denormalizer->denormalize($data, $type, $format, $context);
		$user = $this->security->getUser();
		if ($user instanceof User) {
			return $obj->setCreatedBy($user);
		}
		throw new UnauthorizedHttpException("", "Invalid user");
	}

}
